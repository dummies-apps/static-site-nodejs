# Static Site Nodejs

## Description

In this example we will build a simple node application that serves static html pages.

We have three versions in this app, in each version we gonna change the background color of the index page. This example serves to show how to use deployments in kubernetes.

## Installation

If you need to get the app without installation, you can get it from docker hub :

    $ docker pull mouhamedali/static-site-nodejs:v1.0.0

The is also two other version , the version 2 (v2.0.0) and the version 3 (v3.0.0).

### Local machine

On the current directory, run this command to build the docker image of the first version :

    $ git checkout tags/v1.0.0

    $ docker build -t mouhamedali/static-site-nodejs:v1.0.0 .

    $ docker push mouhamedali/static-site-nodejs:v1.0.0

don't forget to change `mouhamedali` with your docker id.

Run the app :

    $ docker run -d -p 8080:8000 mouhamedali/static-site-nodejs:v1.0.0

You can access the app via this link : http://localhost:8080

And get the version using this : http://localhost:8080/version

- second version

```sh
$ git checkout tags/v2.0.0
```

    $ docker build -t mouhamedali/static-site-nodejs:v2.0.0 .

    $ docker push mouhamedali/static-site-nodejs:v2.0.0

- third version

```sh
$ git checkout tags/v3.0.0
```

    $ docker build -t mouhamedali/static-site-nodejs:v3.0.0 .

    $ docker push mouhamedali/static-site-nodejs:v3.0.0

Do the same tests as the first version.

### Push it on GCR (Google Container Registry)

On the current directory, run this command to build the docker image :

    $ docker tag  mouhamedali/static-site-nodejs:v1.0.0 eu.gcr.io/[YOUR-PROJECT-ID]/static-site-nodejs:v1.0.0

To get the list of your projects ids, run this command :

    $ gcloud projects list

Now, run this command to push the image (you have to log in before) :

    $ gcloud docker -- push eu.gcr.io/[YOUR-PROJECT-ID]/static-site-nodejs:v1.0.0

Do the same for any other version.

You can check this url to see if the images has been pushed correctly :

- [google container registry](https://console.cloud.google.com/gcr)
