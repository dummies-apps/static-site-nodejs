const express = require("express"),
  app = express(),
  path = require("path");

const version = "static-site-nodejs:v3.0.0";
const color = "green";

// to serve static routes. for example if in your html file you are using css files. you have to use this configuration
// check the index.html file to see how to import the main.css file
app.use(express.static(path.join(__dirname, "public")));

app.get("/version", function (req, res) {
  res.send(`{
    color: ${color},
    version: ${version}
  }`);
});

//__dirname : It will resolve to your project folder.
app.use((req, res, next) => {
  res.status(200).sendFile(path.join(__dirname, "views", "index.html"));
});

app.listen(8000, function () {
  console.log(`${version} is listening on port 8000 !!!`);
});
